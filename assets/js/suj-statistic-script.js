var margin = {
    top: 30,
    right: 30,
    bottom: 30,
    left: 30
};
var width = 430;
var height = 280;

var parseDate = d3.time.format("%b").parse;

var x = d3.time.scale().range([0, width]);
var y = d3.scale.linear().range([height, 0]);

var xAxis = d3.svg.axis().scale(x)
    .orient("bottom").ticks(5);

var yAxis = d3.svg.axis().scale(y)
    .orient("left").ticks(5);

var valueline = d3.svg.line()
    .x(function (d) {
      return x(d.date);
    })
    .y(function (d) {
      return y(d.close);
    })
    .interpolate("basis");;

var svg = d3.select(".statistic-wrapper .card-content")
    .append("svg")
    .attr("width", width + margin.left + margin.right)
    .attr("height", height + margin.top + margin.bottom)
    .append("g")
    .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

// Get the data
/*var data = [{
    date: "Mar",
    close: "58.13"
}, {
    date: "Apr",
    close: "53.98"
}, {
    date: "May",
    close: "67.00"
}, {
    date: "Jun",
    close: "89.70"
}, {
    date: "Jul",
    close: "79.00"
}];*/

d3.json("data.json", function(error, data) {

data.forEach(function (d) {
    d.date = parseDate(d.date);
    d.close = +d.close;
});

// Scale the range of the data
x.domain(d3.extent(data, function (d) {
    return d.date;
    }));
y.domain([0, d3.max(data, function (d) {
    return d.close;
    })]);

svg.append("path") // Add the valueline path.
.attr("d", valueline(data));

svg.append("g") // Add the X Axis
.attr("class", "x axis")
    .attr("transform", "translate(0," + height + ")")
    .call(xAxis);

svg.append("g") // Add the Y Axis
.attr("class", "y axis")
    .call(yAxis);

}


d3.json('https://canvasjs.com/services/data/datapoints.php?xstart=1&ystart=10&length=10&type=json' + id, function(data) {
    console.log(data.json);
});