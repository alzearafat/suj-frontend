


$(document).ready(function(){
  //Navbar Collapse JS
  $(".button-collapse").sideNav();
  //EOL

  //Dropdown Materialize JS
  $(".dropdown-button").dropdown();
  //EOL

  //Dropdown SideNav Materialize JS
  $("#suj-dashboard-nav .dropdown-button").dropdown({
    stopPropagation: false
  });
  //EOL

  //Collapsible Nav
  $('.collapsible').collapsible();
  //EOL

  //Select Initialization
  $('select.select-active').material_select();
  //EOL

  //Keyword Tag Chips Initialization == Dashboard - Post Job
  $('.chips').material_chip();
  $('.chips-initial').material_chip({
    data: [{
      tag: 'web developer',
    }, {
      tag: 'startup',
    }],
  });

  $('.chips-initial-skill').material_chip({
    data: [{
      tag: 'marketing',
    }],
  });
  //EOL

  //Skill Tag Chips Initialization == Job & Recruiting - Subscribe Resume
  $('.chips-candidate-skills').material_chip({
    data: [{
      tag: 'marketing',
    }, {
      tag: 'design',
    }],
  });
  //EOL

  //Categories Tags == Contribute Article 
  $('.categories-autocomplete').material_chip({
    data: [{
      tag: 'Uncategorized',
    }],
    autocompleteOptions: {
      data: {
        'Technology': null,
        'Recruitment': null,
        'Uncategorized': null
      },
      limit: Infinity,
      minLength: 1
    }
  });
  //EOL

  // Initialize Modal
  $('.modal').modal();
  //EOL


  //SUJ Initialize Alert
  //$(".suj-alert").alert();
  $(".suj-alert-close").click(function(){
      $(this).parent().hide(300);
  });
  //EOL


  //SUJ Dashboard - Settings User Management - Delete Member
  $("#del_member_yes_1").click(function() {
      $("#member_id_1").remove();
  });
  //EOL

  //SUJ Dashboard - Settings Purchase Credit - Delete Order
  $("#del_purchase_yes_1").click(function() {
      $("#purchase_content_1").remove();
  });
  //EOL


  //SUJ Dashboard - Event Date Picker
  $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 15, // Creates a dropdown of 15 years to control year,
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: false // Close upon selecting a date,
  });
  //EOL

  //SUJ Dashboard - Event Time Picker
  $('.timepicker').pickatime({
    default: 'now', // Set default time: 'now', '1:30AM', '16:30'
    fromnow: 0,       // set default time to * milliseconds from now (using with default = 'now')
    twelvehour: false, // Use AM/PM or 24-hour format
    donetext: 'OK', // text for done-button
    cleartext: 'Clear', // text for clear-button
    canceltext: 'Cancel', // Text for cancel-button
    autoclose: false, // automatic close timepicker
    ampmclickable: true, // make AM PM clickable
    aftershow: function(){} //Function for after opening timepicker
  });
  //EOL


  //Quill Editor
  $('.quill-editor').each(function () {
      var container = $(this).get(0);
      var options = {
          readOnly: $(this).data('readonly') || false,
          theme: $(this).data('theme') || 'snow'
      };
      var editor = new Quill(container, options);

      $(this).data('quill-editor', editor);

  });
  //EOL


  //Image Pop Up JS
  $('.materialboxed').materialbox();
  //EOL

});


//SUJ Dashboard Article Equal Height JS == Dashboard Page
var maxHeight = 0;

$(".suj-event-slider-content li .card").each(function(){
   if ($(this).height() > maxHeight) { maxHeight = $(this).height(); }
});

$(".suj-event-slider-content li .card").height(maxHeight);
//EOL


//Preview Upload Image JS == Dashboard - Profile
function readURL(input, id) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $(id)
                .css('background', 'transparent url('+e.target.result +') 0 0/cover no-repeat');
        };

        reader.readAsDataURL(input.files[0]);
    }
}
//EOL


//Purchase Add Order Quantity JS
var total;
// if user changes value in field
$('.purchase-quantity-field').change(function() {
  // maybe update the total here?
}).trigger('change');

$('.purchase-quantity-add').click(function() {
  var target = $('.purchase-quantity-field', this.parentNode)[0];
  target.value = +target.value + 1;
});

$('.purchase-quantity-sub').click(function() {
  var target = $('.purchase-quantity-field', this.parentNode)[0];
  if (target.value > 0) {
    target.value = +target.value - 1;
  }
});
//EOL
